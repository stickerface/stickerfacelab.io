<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>StickerFace Project Documentation</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <link rel="icon" type="image/png" href="img/favicon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32.png">
    <link rel="icon" type="image/png" sizes="64x64" href="img/favicon-64.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96.png">
  </head>
  <body>


  <nav>
    <div style="vertical-align: middle;">
      <ul>
        <li><a href="#presentation">Presentation</a></li>
        <li>
          <a href="#content">Content</a>
          <ul>
            <li><a href="#supported_versions">Supported Versions</a></li>
            <li><a href="#installing_the_maya_tools">Installing the Maya Tools</a></li>
            <li><a href="#building_the_vray_plugin">Building the VRay Plugin</a></li>
          </ul>
        </li>
        <li><a href="#emoting_with_stickerface">Emoting with StickerFace</a></li>
        <li>
          <a href="#facial_autorig">Facial Autorig</a>
          <ul>
            <li><a href="#using_the_wizard">Using the Wizard</a></li>
            <li><a href="#tweaking_the_rig">Tweaking the Rig</a></li>
          </ul>
        </li>
        <li>
          <a href="#picker_for_animation">Picker for Animation</a>
          <ul>
            <li><a href="#baking_the_animation">Baking the Animation</a></li>
          </ul>
        </li>
        <li>
          <a href="#rendering_with_vray">Rendering with VRay</a>
          <ul>
            <li><a href="#color_correction">Color Correction</a></li>
            <li><a href="#changing_resources">Changing Resources</a></li>
            <li><a href="#final_step">Final Step</a></li>
            <li>
              <a href="#troubleshooting">Troubleshooting</a>
              <ul>
                <li><a href="#lost_face_shape">Lost Face Shape?</a></li>
                <li><a href="#no_rendered_sprites">No Rendered Sprites?</a></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>

  <main>
  <header>
    <h1>StickerFace: A Toolkit for Sprite-Based Facial Animation</h1>
  </header>


  <section>
    <h2><a id="presentation"></a>Presentation</h2>

    <p>
      <em>StickerFace</em> is an opensource project providing users with the set of scripts and plugins needed to rig, animate, and render cartoon-inspired facial expressions with Maya and VRay.
    </p>

    <p>
      Built with earlier versions of Autodesk Maya in mind, StickerFace heavily relies on the hardware acceleration made achievable by Maya's Viewport 2.0, and achieves better performances
      than earlier implementations of Maya's native projection nodes.
    </p>

    <p>
      StickerFace's core features are its CGFX shaders used to provide animators with previews of the final facial expressions at real-time rates.
      But besides these GPU-enhanced materials, StickerFace also comes with scripts facilitating their use at all the relevant steps of the production process :
    </p>
    <ul>
      <li>
        The auto-rigging procedure allows users to straightforwardly apply those shaders to any geometry, configure its behavior at the animation stage, and create a fully functional controller hierarchy.
      </li>
      <li>
        The controller picker greatly speeds up the animation process by displaying all sprites available per facial feature and providing tools to globally edit expressions.
      </li>
      <li>
        Finally, we implemented tailored VRay nodes that guarantee the exact reproduction at the rendering stage of the facial expressions designed by animators, and scripted the construction of the necessary shading network.
      </li>
    </ul>

    <p>
      Each aforementioned script or tool is presented to users via an intuitive GUI that can be invoked through Maya shelves for quicker access.
    </p>
  </section>


  <section>
    <h2><a id="content"></a>Content</h2>

    <p>
      Developed at <a href="http://www.supamonks.com">SUPAMONKS STUDIOS</a>, StickerFace's source code has since been released as an opensource project under the LGPL3 license.
    </p>

    <p>
      The StickerFace Git repository is available <a href="https://gitlab.com/stickerface/stickerface">here</a>.
    </p>

    <p>
      The repository stores the entirety of the source code needed to build and manipulate all the entities required by the facial animation workflow, from auto-rigging scripts to dedicated VRay nodes used during rendering. Its folder structure splits into two distinct hierarchies:
    </p>
    <ul>
      <li>
        The <a href="https://gitlab.com/stickerface/stickerface/tree/master/maya">maya subfolder</a> stores the Python source code responsible for the auto-rigging system, picker, and the construction of the shading network after animation baking.
      </li>
      <li>
        The <a href="https://gitlab.com/stickerface/stickerface/tree/master/vray">vray subfolder</a> contains the C++ implementation of our custom textures as a VRay plugin.
      </li>
    </ul>

    <article>
      <h3><a id="supported_versions"></a>Supported Versions</h3>

      <ul>
        <li>
          As previously stated, StickerFace is mostly intended as a replacement for earlier implementations of Maya's projection nodes. It has been tested with Maya 2015 and Maya 2018.
        </li>
        <li>
          At the time of writing, the VRay plugin has only be tested on Windows x64 platforms, and built with Visual Studio 2012. The version of the VRay App SDK used is 3.25.01 (<code>VRAY_DLL_VERSION = 0x32501</code>).
        </li>
      </ul>
    </article>

    <article>
      <h3><a id="installing_the_maya_tools"></a>Installing the Maya Tools</h3>

      <p>
        Once cloned or downloaded from the <a href="https://gitlab.com/stickerface/stickerface">Git repository</a>, StickerFace's Maya tools only requires a few additional installation steps:
      </p>
      <ul>
        <li>
          Your local version of the project's <a href="https://gitlab.com/stickerface/stickerface/tree/master/maya/stickerface/plugin">Maya plugin folder</a> must appear in your <code>MAYA_PLUG_IN_PATH</code> environment variable.
        </li>
        <li>
          The StickerFace's toolbox can be installed by a simple drag-and-drop of the <code>stickerface_install.mel</code> script file onto a running Maya.
        </li>
      </ul>
      <div style="text-align: right;">
        <img src="img/install.gif"/>
      </div>
    </article>

    <article>
      <h3><a id="building_the_vray_plugin"></a>Building the VRay Plugin</h3>

      <p>
        Building the VRay plugin is slightly more involving, and in its current state, compiling its source code has a couple of requirements:
      </p>
      <ul>
        <li>Visual Studio 2012 is required to produce the VRay plugin's binary.</li>
        <li>Two external dependencies are also needed to build the plugin:
          <ul>
            <li>the header-only library for linear algebra, <a href="http://eigen.tuxfamily.org">Eigen</a>;</li>
            <li>and naturally, the <a href="https://www.chaosgroup.com/vray/application-sdk">VRay application SDK</a>.</li>
          </ul>
        </li>
        <li>The build system relies on <a href="https://premake.github.io">Premake5</a> and prior its invocation, the paths to the two externals dependencies must be filled in the <code>path.lua</code> file (by setting the <code>EIGEN_ROOT</code> and <code>VRAY_SDK_ROOT</code> variables). </li>
      </ul>

      <p>
        The Visual Studio solution used to compile the VRay plugin is generated by running the following command:
      </p>
      <div class="cmd">
        <code>premake5.exe vs2012</code>
      </div>

      <p>
        Finally, the path to your compiled binary <code>vray_stickerface.dll</code> must appear in the environment variable used by VRay when searching for plugins to load (<code>VRAY_FOR_MAYA2015_PLUGINS_x64</code> when working with Maya 2015 for instance).
      </p>
    </article>
  </section>


  <section>
    <h2><a id="emoting_with_stickerface"></a>Emoting with StickerFace</h2>

    <p>
      A face created with StickerFace consists in the collection of texture sprites whose layering and projection are executed by a CGFX material. All facial features are then represented by sprites drawn from a common <em>sprite atlas</em>. An atlas is an image file made of the juxtaposition of all possible sprites, aligned in a regular grid layout.
    </p>

    <p>
      At a given time, the controller of each facial feature supplies the index of the sprite to be displayed. For easier use, it is possible to assign a different validity range and default value to each facial feature.
    </p>

    <p>
      A face is made up of eleven facial features and three extra sprites that can be used to create richer expressions. It should be noted that, beside layer ordering, some sprites can only appear through others: Pupil and eyelid sprites are expected to lie inside the sprites dedicated to eyeballs and will be cropped otherwise.
    </p>

    <div style="text-align: right;">
      <img src="img/ctrl_shapes_public.svg"/>
    </div>

    <p>
      Above lies an image showing the controllers associated with the different facial features, as well as the identifiers the StickerFace system uses to refer to them.
    </p>
  </section>


  <section>
    <h2><a id="facial_autorig"></a>Facial Autorig</h2>

    <p>
      Using StickerFace to adorn a shape with a facial rig is a two-fold process:
    </p>
    <ol>
      <li>Run the StickerFace autorig process and feed the GUI wizard with the information it expects (such as the face geometry, the projection type, the paths to the generated shader and input textures, and the general sprite layout information). The CGXF material, the controller hierarchy, as well as the DG network connecting the two will automatically be created.</li>
      <li>Riggers can then manually finetune sprite projections or controller placements prior animation if needed.</li>
    </ol>

    <div class="note">
      The StickerFace system assumes the face geometry to be z-forward facing.
    </div>

    <article>
      <h3><a id="using_the_wizard"></a>Using the Wizard</h3>

      <p>
        The autorig wizard is summoned by clicking the <img class="icon" src="img/icon/icon_autorig.png"> icon on the Maya shelves. The following dialog shall appear:
      </p>

      <div style="text-align: right;">
        <img src="img/gui_autorig_01.png" />
      </div>

      <p>
        This first window gathers all mandatory information for performing automatic facial rigging on a given shape, as well as a couple of optional controls. Here is the meaning behind its icons:
      </p>
      <ul>
        <li>
          <img class="icon" src="img/icon/asset.png"/> represents the asset and should always be associated with a relevant name.
        </li>
        <li>
          <img class="icon" src="img/icon/face_shape.png"/> stands for the shape the facial rig and sprites will be applied to. You initialize it from the selection by clicking the <img class="icon" src="img/icon/select_shape.png"/> icon on the right.
        </li>
        <li>
          Currently, StickerFace handles three types of geometric projections for applying the sprites onto the shape geometry. Depending on its overall shape, different projections can yield more intuitive and controllable results. You choose your desired projection by checking one of the following icons:
          <ul>
            <li><img class="icon" src="img/icon/sphere.png"/> for the spherical projection, </li>
            <li><img class="icon" src="img/icon/cube.png"/> for the cubic projection, </li>
            <li><img class="icon" src="img/icon/plane.png"/> for the planar projection. </li>
          </ul>
          Once done, you specify the path the CGFX shader must be saved to by clicking the <img class="icon" src="img/icon/save.png"/> icon.
        </li>
        <li>
          <img class="icon" src="img/icon/sprite_sheet.png"/> corresponds to the sprite atlas, whose path is specified by clicking the <img class="icon" src="img/icon/browse_location.png"/> icon. The sprite atlas corresponds to a file image storing <em>all</em> sprite disposed in a regular grid layout.
        </li>
        <li>
          <img class="icon" src="img/icon/body_texture.png"/> designates the body texture, which is the texture that will appear below the projected sprites. Its mapping onto the surface is dictated by the texture coordinates assigned to the vertices of the geometry. Its path is specified by clicking the <img class="icon" src="img/icon/browse_location.png"/> icon.
        </li>
      </ul>

      <p>
        The lower half of the dialog is reserved for optional controls over the generated facial rig:
      </p>
      <ul>
        <li>
          <img class="icon" src="img/icon/parent_constraint.png"/> represents the source object of an optional parent constraint targeting the generated rig's root transform. It can be initialized from the selection by clicking the <img class="icon" src="img/icon/select_obj.png"/> icon on the right. By default, a parent constraint involving the face shape's transform is created.
        </li>
        <li>
          <img class="icon" src="img/icon/scaling.png"/> indicates which external attribute will serve as a dynamic global scaling factor on all of the sprite projections. By default, no such connection is created, but users can specify any scalar attribute from the selection for that purpose by clicking the <img class="icon" src="img/icon/select_attr.png"/> icon and picking one of its eligible attributes.
        </li>
      </ul>

      <div class="warning">
        <a id="warning_assetname"></a>
        For the system to work in its entirety, you must name your asset (by giving your Maya scene a filename). This name is especially important for the StickerFace workflow to keep on correctly identifying your face geometries once your animation has been baked.
        <div style="text-align: right;">
          <div class="images">
            <img src="img/warning_asset_01.png"/>
          </div>
          <div class="images">
              <img src="img/warning_asset_02.png"/>
          </div>
        </div>
      </div>

      <p>
        Once users have provided the required information, clicking the <img class="icon" src="img/icon/right.png"/> icon on the bottom right corner brings the following dialog if a valid sprite atlas has been located:
      </p>
      <div style="text-align: right;">
        <img src="img/gui_autorig_02.png"/>
      </div>
      <p>
        This step is optional and skippable, but it greatly facilitates the specification of the information required for the sprite atlas to be correctly handled by the StickerFace system. It provides visual support for the specification of the following:
      </p>
      <ul>
        <li>
          <em>The resolution of the atlas grid</em><br>
          The number of columns and rows of the grid sustaining the sprite atlas should be properly set. The resulting grid layout is shown with white dashed lines.
        </li>
        <li>
          <em>The range for each facial feature</em><br>
          Users can restrict the range of sprite indices each facial feature can accept: by checking one of the available feature buttons displayed on the right, and left-clicking the desired first and last sprite locations on the atlas grid displayed on the left. The range of the currently selected feature is highlighted with light blue outlines.
        </li>
        <li>
          <em>The default sprite for each facial feature</em><br>
          The default index value for each facial feature is specified by right-clicking the corresponding sprite location on the atlas grid. It is then decorated with the <img class="icon" src="img/icon/star.png"/> symbol.
        </li>
      </ul>

      <p>
        Since the previous step is optional, all the information it sets can be subsequently modified if needed, either by editing the values of the attributes attached to the <code>face_anim_root</code> controller (index ranges and default values, as well as scaling factors) or the generated material itself (atlas grid resolution):
      </p>
      <div style="text-align: right;">
        <div class="images">
          <img src="img/root_attribs.png"/>
        </div>
        <div class="images">
            <img src="img/mat_attribs.png"/>
        </div>
      </div>

      <p>
        Finally, clicking the <img class="icon" src="img/icon/exec.png"/> icon on the right bottom corner triggers the generation of the facial rig.
      </p>

      <p>
        Worth noticing from the DAG object hierarchy output by the autorig procedure are:
      </p>
      <ul>
        <li>the root group, <code>face_anim_root</code>, targeted by a parent constraint and storing many extra attributes relevant to riggers,</li>
        <li>the hierarchy of transforms starting at the <code>face_anim_null</code> group, inside of which the controller shapes live,</li>
        <li>a proxy object used for animation baking.</li>
      </ul>

      <p>
        The transform hierarchy contains different categories of subgroups, each with its specific purpose:
      </p>
      <ul>
        <li>the <code>*_anim</code> transforms are reserved for animating the sprites' projections,</li>
        <li>the <code>*_anim_null</code> groups are used to adjust the sprites' projections onto the face shape prior animation in order to create a proper neutral expression,</li>
        <li>the <code>*_curve_null</code> groups enable the repositioning of the controllers' shapes without altering the sprites' projections,</li>
        <li>the <code>*_connected_null</code> groups are used internally by the system and are not supposed to be edited.</li>
      </ul>

      <div style="text-align: right;">
        <img src="img/ctrl_outliner.png"/>
      </div>
    </article>

    <article>
      <h3><a id="tweaking_the_rig"></a>Tweaking the Rig</h3>

      <p>
        Once the wizard has been successfully run, a dedicated material has been assigned to the face geometry, animation controllers have been generated, connected to its attributes, and placed within a specific hierarchy of transforms (as well as in a separate selection set). The facial rig should be functional, but it is most likely that manual adjustments will be needed for it to be usable at the animation stage.
      </p>

      <p>
        For better efficiency, editing the facial rig should follow a specific workflow:
      </p>
      <ol>
        <li>
          <p>
            Place the overall controller hierarchy at a decent distance to the face geometry by translating the <code>face_anim_null</code> group along the z-axis.
          </p>
          <div style="text-align: right;">
            <div class="images">
                <img src="img/maya_keke_persp_translateZ.gif"/>
            </div>
            <div class="images">
                <img src="img/maya_keke_left_translateZ.gif"/>
            </div>
          </div>
          <div class="note">
            This step should be done as early as possible since some projections depend on the distance of the controllers to the geometry (i.e. spherical projections). In such instances, moving the controller hierarchy around later on is likely to force users to redo the manual tweaking of the controllers.
          </div>
        </li>

        <li>
          <p>
            Calibrate the sprites' projections on the face by adjusting the <code>*_anim_null</code> groups' translation and scaling values.
          </p>
          <div style="text-align: right;">
            <div class="images">
                <img src="img/maya_keke_persp_01.gif"/>
            </div>
            <div class="images">
                <img src="img/maya_keke_front_face.gif"/>
            </div>
          </div>
        </li>

        <li>
          <p>
            Only after you obtained a proper neutral facial expression, can you adjust the controllers' positions independently of the sprites' projections, by tweaking the <code>*_curve_anim</code> groups' transforms.
          </p>
          <div style="text-align: right;">
            <div class="images">
                <img src="img/maya_keke_persp_02.gif"/>
            </div>
            <div class="images">
                <img src="img/maya_keke_front_ctrls.gif"/>
            </div>
          </div>
        </li>
      </ol>

      <div class="warning">
        At the end of the facial rigging process, no <code>*_anim</code> controllers should have a transform value differing from the identity.
      </div>
    </article>
  </section>


  <section>
    <h2><a id="picker_for_animation"></a>Picker for Animation</h2>

    <p>
      Each facial feature is associated with a controller. Its world position dictates where its corresponding sprite projects onto the face geometry, and one of its extra attribute stores the index that determines which sprite from the atlas should be displayed.
    </p>

    <div style="text-align: right;">
      <img src="img/anim_attribs.png" />
    </div>

    <p>
      While keyframing the controllers' sprite indices could be done via Maya's attribute editor, the animation process can be made much smoother by using the <em>sprite picker</em> that can be invoked by clicking the <img class="icon" src="img/icon/icon_picker.png"/> icon from the Maya shelves.
    </p>

    <div style="text-align: right;">
      <img src="img/gui_picker.png"/>
    </div>

    <p>
      For a given asset, the sprite picker straightforwardly lays out thumbnails corresponding to the sprites each of the asset's facial feature can display.
    </p>
    <ul>
      <li>
        Clicking a thumbnail changes its corresponding controller's sprite index and effectively alters the facial expression. Currently displayed sprites are highlighted by brightly colored squares, with red squares indicating that a key has been set at the current frame.
      </li>
      <li>
        When a facial feature can be represented by many sprites, holding ALT and the mouse's middle button enables horizontal scrolling through its thumbnail gallery.
      </li>
      <li>
        A single mouse click inside the area dedicated to a specific animation controller selects it.
      </li>
      <li>
        When a scene includes multiple assets with a StickerFace rig, users can have the picker switch between them via the <img class="icon" src="img/gui_picker_asset.png"/> asset choicer. Asset detection is triggered by clicking the <img class="icon" src="img/icon/reload.png"/> icon.
      </li>
    </ul>

    <p>
      Scripts are also available to speed up some of the most frequent face edits:
    </p>
    <ul>
      <li>
        <img class="icon" src="img/icon/reset.png"/> brings the asset's face back to its default configuration.
      </li>
      <li>
        <img class="icon" src="img/icon/mirror.png"/> mirrors the asset's facial expression.
      </li>
      <li>
        <img class="icon" src="img/icon/symm_R_to_L.png"/> and <img class="icon" src="img/icon/symm_L_to_R.png"/> symmetrize the asset's facial expression.
      </li>
    </ul>

    <article>
      <h3><a id="baking_the_animation"></a>Baking the Animation</h3>

      <p>
        Beside the controller hierarchy, running the StickerFace autorig triggers the creation of a <em>proxy object</em>.
      </p>

      <p>
        This object is used as a bridge between the animation scene and the lighting scene. Each asset is associated with its own proxy object. This proxy object stores the animation curves of all the facial features' controllers as extra attributes, as well as the information mandatory for the later stages of the StickerFace system to work.
      </p>

      <div style="text-align: right;">
        <div class="images">
            <img src="img/proxy_outliner.png"/>
        </div>
        <div class="images">
            <img src="img/proxy_attribs.png"/>
        </div>
      </div>

      <p>
        All the names of the proxy's attributes start with the <code>smks_</code> prefix. This prefix is used when baking the animation scene in an Alembic file to filter out irrelevant attributes.
      </p>

      <div style="text-align: right;">
        <div class="images">
            <img src="img/proxy_alembic.png"/>
        </div>
        <div class="images">
            <img src="img/proxy_prefix.png"/>
        </div>
      </div>
    </article>
  </section>


  <section>
    <h2><a id="rendering_with_vray"></a>Rendering with VRay</h2>

    <p>
      For rendering animated facial expressions from baked scenes, one must make sure the proxy objects of all involved assets, as well as all their extra attributes, have been properly saved in the Alembic file.
    </p>

    <p>
      After importing their Alembic archives to a new Maya scene, users simply need to run the last wizard from the StickerFace toolbox that is accessible by clicking on the <img class="icon" src="img/icon/icon_vray.png"/> icon on the shelf. Upon invocation, it will attempt to locate as many proxy objects as possible in the scene, and interpret all the available information related to them. The wizard will display its findings via <em>proxy cards</em> as follows:
    </p>

    <div style="text-align: right;">
      <img src="img/gui_vray.png"/>
    </div>

    <p>
      Among the recognized pieces of information are:
    </p>
    <ul>
      <li>
        the asset's unique name <img class="icon" src="img/icon/asset.png"/>
      </li>
      <li>
        the type of geometric projection used by the facial rig <img class="icon" src="img/icon/sphere.png"/>, <img class="icon" src="img/icon/cube.png"/>, or <img class="icon" src="img/icon/plane.png"/>
      </li>
      <li>
        the face shape's name <img class="icon" src="img/icon/face_shape.png"/>
      </li>
      <li>
        the proxy object's name <img class="icon" src="img/icon/proxy.png"/>
      </li>
      <li>
        the sprite atlas's path used during animation <img class="icon" src="img/icon/sprite_sheet.png"/>
      </li>
      <li>
        the body texture's path used during animation <img class="icon" src="img/icon/body_texture.png"/>
      </li>
    </ul>

    <p>
      Users can manually trigger the proxy object detection by clicking the <img class="icon" src="img/icon/reload.png"/> icon.
    </p>

    <article>
      <h3><a id="color_correction"></a>Color Correction</h3>

      <p>
        Applying gamma correction to input textures is usually necessary prior rendering. The wizard proposes to facilitate this process by automatically adding a <code>gammaCorrect</code> Maya node inside the shading network it generates. Its initial value can be directly set via the wizard in the text field next to the <img class="icon" src="img/icon/gamma.png"/> icon.
      </p>

      <div class="note">
        Since the VRay plugins responsible for rendering the facial expressions handle the loading of the sprite atlas themselves, they also expose a set of special attributes to perform gamma correction. In order to maintain the correction values applied to the body texture and the atlas consistent, it is strongly recommended to edit the values from the <code>gammaCorrect</code> Maya node generated by the wizard.
      </div>
    </article>

    <article>
      <h3><a id="changing_resources"></a>Changing Resources</h3>

      <p>
        When using different sets of textures between the animation and rendering stages (e.g. different resolutions), the rendering wizard allows users to change paths for both the sprite atlas and the body texture. This can be done on an individual basis by clicking the <img class="icon" src="img/icon/browse_location.png"/> icons beside all paths, or by globally remapping resource path hierarchies by clicking the <img class="icon" src="img/icon/replace_folder.png"/> icon.
      </p>

      <p>
        Clicking the <img class="icon" src="img/icon/replace_folder.png"/> icon brings the following dialog that users can use to globally swap between file hierarchy roots:
      </p>

      <div style="text-align: right;">
        <img src="img/gui_vray_replace.png"/>
      </div>

      <ul>
        <li>
          <img class="icon" src="img/icon/src_folder.png"/> represents the <em>source folder</em> whose path will be replaced in all texture filepaths. It is set by clicking the <img class="icon" src="img/icon/browse_location.png"/> icon.
        </li>
        <li>
          <img class="icon" src="img/icon/dst_folder.png"/> stands for the <em>destination folder</em> whose path will replace instances where the source folder's path appears in the texture files. It is set by clicking the <img class="icon" src="img/icon/browse_location.png"/> icon.
        </li>
        <li>
          The resource remapping process is triggered by clicking the <img class="icon" src="img/icon/exec.png"/> icon.
        </li>
      </ul>

      <div class="warning">
        At the time of writing, only PNG image files can serve as sprite atlases for rendering with VRay.
      </div>
    </article>

    <article>
      <h3><a id="final_step"></a>Final Step</h3>

      <p>
        Once all proxy objects have been detected, their gamma correction values set, and the paths of their resource textures possibly updated, users can click the <img class="icon" src="img/icon/vray.png"/> icon to generate the shading network needed to render the StickerFace expressions with VRay.
      </p>

      <div class="note">
        Upon execution, the shading network generating script loads the <code>vrayformaya</code> plugin, along with all the VRay plugins the system can find in the process. For that reason, first executions can appear suspiciously slow.
      </div>

      <p>
        Below is an example of a character rendered with StickerFace expressions using the VRay renderer:
      </p>
      <div style="text-align: right;">
        <div class="images">
            <img src="img/renderview.png"/>
        </div>
        <div class="images">
            <img src="img/maya_keke_vray.gif"/>
        </div>
      </div>
    </article>

    <article>
      <h3><a id="troubleshooting"></a>Troubleshooting</h3>

      <h4><a id="lost_face_shape"></a>Lost Face Shape?</h4>
      <div class="warning">
        <a href="#warning_assetname">As previously mentioned</a>, the asset name is a crucial information that must be preserved throughout the entire process. It is used to uniquely identify and bring together entities that must work together. <br><br>
        The following screenshot showcases an occurence of an undetected shape at the rendering step due to mismatching or missing asset name information:
        <div style="text-align: right;">
          <img src="img/trouble_missing_shape.png">
        </div>
        In such cases, users must check the correctness of the asset name information seen by the system. It is attached as an extra attribute to the following objects:
        <ul>
          <li>
            on the shape itself,
          </li>
          <li>
            on the <code>face_anim_root</code> group at the top of the controller hierarchy,
          </li>
          <li>
            on the proxy object.
          </li>
        </ul>
      </div>

      <h4><a id="no_rendered_sprites"></a>No Rendered Sprites?</h4>
      <div class="warning">
        Passing keyed attributes to VRay plugins appears to be a fragile process. <br><br>
        The facial attributes (individual sprites' indices and transforms), as well as their animation curves, are stored as extra attributes attached to the proxy object created by the autorig. These are the inputs used by the VRay textures, and the StickerFace solution expects you to run its VRay wizard in a scene importing the baked animation stored in an Alembic file. <br><br>
        When directly generated in the animation scene, the shading network may encounter updating issues. Such instances can be detected by examining the Maya output window that displays the attribute values effectively used by the VRay renderer. <br><br>
        Follow illustrations of incorrectly initialized sprite mapping values (on the left) and properly updated values (on the right):
        <div style="text-align: right;">
          <div class="images">
              <img src="img/output_window_issue.png"/>
          </div>
          <div class="images">
              <img src="img/output_window_solved.png"/>
          </div>
        </div>
      </div>
    </article>

  </section>

  <footer>
    <h4>
      Code and documentation by <a href="http://pierre-landes.net">Pierre-Edouard LANDES</a> for <a href="http://www.supamonks.com"><img style="width: auto; height: 1.5em; vertical-align: text-bottom;" src="img/logo_supamonks.png"/></a>
    </h4>
  </footer>
  </main>
  </body>
</html>

